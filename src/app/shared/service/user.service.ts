import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { ConfigService } from "./config.service";

import { BehaviorSubject } from "rxjs/Rx";
import { Observable } from "rxjs/Rx";

@Injectable()

export class UserService {
    private baseUrl: string;

    private authNavStatusSource = new BehaviorSubject<boolean>(false);

    authNavStatus$ = this.authNavStatusSource.asObservable();

    constructor(private http : Http, private config: ConfigService) {
        var loggedIn = !!localStorage.getItem('auth_token');

        this.authNavStatusSource.next(loggedIn);

        this.baseUrl = config.getApiURI();
    }

    public login(username: string) {
        return this.http.post(this.baseUrl+'/auth/login', JSON.stringify({username}))
            .map(res => res.json())
            .map(res => {
                localStorage.setItem('auth_token', res.auth_token);
                this.authNavStatusSource.next(true);
                return true;
            })
            .catch(this.handleError);
    }

    private handleError(error: any) {
        var applicationError = error.headers.get('Application-Error');

        // either applicationError in header or model error in body
        if (applicationError) {
            return Observable.throw(applicationError);
        }

        var modelStateErrors: string = '';
        var serverError = error.json();

        if (!serverError.type) {
            for (var key in serverError) {
            if (serverError[key])
                modelStateErrors += serverError[key] + '\n';
            }
        }

        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;

        return Observable.throw(modelStateErrors || 'Server error');
    }
}