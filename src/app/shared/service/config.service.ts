import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
    private apiURI = 'http://localhost:5000/api';

    constructor() { }

    getApiURI() {
        return this.apiURI;
    }
}