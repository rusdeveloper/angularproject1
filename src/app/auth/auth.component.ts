import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../shared/service/user.service';
import { Subscription } from 'rxjs';
import { User } from '../shared/models/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-component',
  templateUrl: './auth.component.html',
})
export class AuthComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  
  errors: string;
  isSubmitted: boolean;

  constructor(private userService: UserService, private router: Router){}

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  
  login({user, valid} : {user: User, valid: boolean}){
    this.isSubmitted = true;
    
    if(valid){
      this.userService.login(user.username).subscribe(result => {
        if(result) { this.router.navigate(["/"])}
      },
      error => this.errors = error);
    }
  }
}
